#!/usr/bin/env nextflow

// enable dsl2
nextflow.enable.dsl=2

// include modules
include {READS_QC} from './modules/assemble.nf'
include {SUBSAMPLE} from './modules/assemble.nf'
include {ASSEMBLE} from './modules/assemble.nf'
include {CIRCLATOR} from './modules/assemble.nf'
include {POLISH} from './modules/assemble.nf'
include {CONTIGS_QC} from './modules/assemble.nf'
include {PROKKA} from './modules/assemble.nf'
include {BAKTA_DOWNLOAD} from './modules/assemble.nf'
include {BAKTA} from './modules/assemble.nf'
include {ROARY} from './modules/assemble.nf'
include {ROARY_PLOTS} from './modules/assemble.nf'
include {FASTTREE} from './modules/assemble.nf'
include {FIGTREE} from './modules/assemble.nf'
include {SPADES} from './modules/assemble.nf'
include {MLST} from './modules/assemble.nf'
include {BANDAGE} from './modules/assemble.nf'
include {MASH} from './modules/assemble.nf'

include {AMRFINDER} from './modules/mge_analysis.nf'
include {PLASMIDFINDER} from './modules/mge_analysis.nf'

workflow {
    // channels
    Channel.fromPath( "${params.inputFastq}/*" )
           .map{ file -> tuple(file.simpleName, file) }
           .set{ ch_fqs }

    Channel.fromPath("${params.inputmodel}")
        .set{ ch_model }

    Channel.fromFilePairs("illumina_fqs/*{1,2}.fastq.gz")
        .map{ row -> tuple(row[0], row[1][0], row[1][1])}
        .set{illumina_fqs}


    main:

    /*
    1) QC raw reads using seqit
    2) Subsample reads to 500x coverage (assuming ~5m genome size)
    3) Assemble using Flye
    4) Use bandage to visualise assemblies (conda env created beforehand)
    5) Combine Flye contigs with assembly info ready for sorting into circular and not
    6) Rotate circular contigs only
    7) Polish Circlator contigs with Medaka
    8) Download Bakta
    9) Use Bakta to annotate genomes
    10) Use Roary on gff3 Bakta outputs
    11) Create plots of Roary outputs
    12) Use Fasttree
    13) Use Figtree
    14) Classify using MLST
    15) Use AMRfinder depending on AMRFINDER classification (either kleb or ecoli)
    16) Use plasmid finder

    To do:
    Maybe make a new workflow which can be run separately excluding poor assemblies
    - add MLST to metadata script output with all 8 columns now
    - add mash to the pipeline
    - merge results

    */

    READS_QC(ch_fqs)

    SUBSAMPLE(ch_fqs)

    ASSEMBLE(SUBSAMPLE.out.fq)

    BANDAGE(ASSEMBLE.out.gfa)

    // Extract info about whether contigs are circular
    circular_contigs_info=ASSEMBLE.out.txt.combine(ASSEMBLE.out.fasta, by:[0])

    // Rotate circular contigs only
    CIRCLATOR(circular_contigs_info)

    CONTIGS_QC(CIRCLATOR.out.fasta)

    POLISH(CIRCLATOR.out.fasta.combine(SUBSAMPLE.out.fq, by:0), ch_model)

    //PROKKA(ASSEMBLE.out.fasta)

    BAKTA_DOWNLOAD()

    SPADES(illumina_fqs)

    contigs=POLISH.out.fasta.mix(SPADES.out.fasta)

    BAKTA(contigs.combine(BAKTA_DOWNLOAD.out))

    gff3s=BAKTA.out.gff3
        .map{ row -> row[1]}
        .collect()

    ROARY(gff3s)

    ROARY_PLOTS(ROARY.out)

    FASTTREE(ROARY.out)

    //FIGTREE(FASTTREE.out)

    MLST(contigs)

    //Merge MLST results
    mlst_rows = MLST.out.csv        
                .map{ it -> it[1]}
    
    // Create a table for reference
    mlst_rows.collectFile(name: 'mlst_table.csv', sort: { file -> file.name }, storeDir: "$params.summaryDir")

    all_contigs=contigs
        .map{ it -> it[1]}
        .collect()

    MASH(all_contigs)

    // AMRFINDER_DOWNLOAD()

    
    // contigs.combine(MLST.out.tsv)

    classified_configs=MLST.out.tsv.combine(contigs, by:[0])

    AMRFINDER(classified_configs, params.amrfinder_db)

    PLASMIDFINDER(contigs)

    // not even sure what this is
    //amr_reports = FIND_AMR.out.map{it -> "\"$it\""}
    //    .toSortedList()
    //COMBINE_AMR(amr_reports, "amrfinder.tsv", params.data_source, "$params.output")

    //Merge ESBL AMRfinder results
    //esbl_rows = AMRFINDER.out.ebsl        
    //            .map{ it -> it[1]} 
    //            .toSortedList()
    //            .view()
    
    // Create a table for reference
    // mlst_rows.collectFile(name: 'mlst_table.tsv', storeDir: "$params.summaryDir")
    

}