// Analysis of mobile genetic elements (MGEs)

process AMRFINDER {
    tag {sample}
    //conda "$params.envs/amrfinder"

    publishDir "${params.outDir}/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy'

    input:
    tuple val(sample), path(tsv_file), path(assembly_file)
    val(db)

    output:
    tuple val(sample), path("${sample}_amrfinder.tsv"), emit: tsv
    tuple val(sample), path("${sample}_amrfinder.csv"), emit: csv
    tuple val(sample), path("${sample}_amrfinder.csv"), emit: esbl

    script:
    """
    if grep -q "ecoli" ${tsv_file}; then
        amrfinder -n ${assembly_file} -d $db -O Escherichia -o ${sample}_amrfinder.tsv --threads ${task.cpus}
    elif grep -q "klebsiella" ${tsv_file}; then
        amrfinder -n ${assembly_file} -d $db -O Klebsiella_pneumoniae -o ${sample}_amrfinder.tsv --threads ${task.cpus}
    else
        amrfinder -n ${assembly_file} -d $db -o ${sample}_amrfinder.tsv --threads ${task.cpus}
    fi

    cat ${sample}_amrfinder.tsv | tr '\\t' ',' > ${sample}_amrfinder.csv

    cat ${sample}_amrfinder.csv | grep "extended-spectrum" | grep "beta-lactamase" > ${sample}_amrfinder_ESBL.csv
    """
    stub:
    """
    touch amrfinder.tsv
    touch amrfinder.csv
    touch amfinder_ESBL.csv
    """
}

/*
// taken from Matthew's serotyping_ecoli pipelinegfr
process COMBINE_AMR {
    conda "$params.envs/pandas"

    publishDir "$outdir", mode: 'copy'

    input:
    val(amr_reports)
    val(outfile)
    val(data_group)
    val(outdir)

    output:
    path("${outfile}")

    script:
    """
    python3 $params.bin/combine_amr.py -a '$amr_reports' -d $data_group -o $outfile
    """
}

// take from genericBugONTWorkflow
process analyse_plasmid {
        label 'notflye'                                                 

    tag { run }
    publishDir 'accessoryResults', mode: 'copy'

    input:
    tuple val(run), path("${run}.O6.blast.txt"), path("${run}.genes.O6.blast.txt")

    output:
    tuple val(run), path("${run}_accessory_results.csv"), emit: details_csv
    tuple val(run), path("${run}_plasmid_coverages.csv"), emit: csv

    script:
    """
    detect_accessory.py -p ${run}.O6.blast.txt -b ${run}.genes.O6.blast.txt -s ${run}
    """

    stub:
    """
    touch ${run}_accessory_results.csv
    touch ${run}_plasmid_coverages.csv
    """

}

*/
/*
PLASMIDFINDER {
    tag {sample}

    publishDir "${params.outDir}", mode: 'copy'

    input:
    tuple val(sample), path(contigs)

    output:
    tuple val(sample), path("plasmidfinder/${sample}")

    script:
    """
    mkdir plasmidfinder/${sample}
    python ${params.neoDir}/bin/plasmidfinder.py -p "${params.plasmidfinder_db}" -i contigs_fasta -o plasmidfinder/${sample}
    """

    stub:
    """
    mkdir plasmidfinder
    """
}

*/

process PLASMIDFINDER {
    tag {sample}

    publishDir "${params.outDir}/plasmidfinder", mode: 'copy'

    input:
    tuple val(sample), path('contigs')

    output:
    tuple val(sample), path("*.json")                 , emit: json
    tuple val(sample), path("*.txt")                  , emit: txt
    tuple val(sample), path("*.tsv")                  , emit: tsv
    tuple val(sample), path("*-hit_in_genome_seq.fsa"), emit: genome_seq
    tuple val(sample), path("*-plasmid_seqs.fsa")     , emit: plasmid_seq


    script:
    """
    python ${params.neoDir}/bin/plasmidfinder.py \\
        -p ${params.plasmidfinder_db} \\
        -i contigs \\
        -o ./ \\
        -x

    # Rename hard-coded outputs with prefix to avoid name collisions
    mv data.json ${sample}_data.json
    mv results.txt ${sample}_results.txt
    mv results_tab.tsv ${sample}_results_tab.tsv
    mv Hit_in_genome_seq.fsa ${sample}-hit_in_genome_seq.fsa
    mv Plasmid_seqs.fsa ${sample}-plasmid_seqs.fsa
    """
}

/*
// take from matthew's ecoli_amr
process RESFINDER {
    conda "$params.envs/resfinder"

    publishDir "$outdir/${barcode}/", mode: 'copy'

    input:
    tuple val(barcode), path(assembly_file)
    val(db)
    val(outdir)

    output:
    tuple val(barcode), path("resfinder")

    script:
    """
    echo $barcode
    python -m resfinder -o resfinder_output -s "ecoli" -db_res $db -ifa $assembly_file -acq
    mkdir resfinder
    cp resfinder_output/pheno_table_escherichia_coli.txt resfinder/pheno_table_escherichia_coli.tsv
    cp resfinder_output/pheno_table.txt resfinder/pheno_table.tsv
    cp resfinder_output/ResFinder_results_tab.txt resfinder/resfinder_results.tsv
    cp resfinder_output/ResFinder_results.txt resfinder/resfinder_report.txt

    abricate --db resfinder $assembly_file > abricate_results.tsv
    mv abricate_results.tsv resfinder/abricate_results.tsv
    """

    stub:
    """
    mkdir resfinder
    touch resfinder/pheno_table_escherichia_coli.tsv
    touch resfinder/pheno_table.tsv
    touch resfinder/resfinder_results.tsv
    touch resfinder/resfinder_report.txt
    touch resfinder/abricate_results.tsv
    """
}

process RESFINDER {
    conda "$params.envs/resfinder"

    publishDir "$outdir/${barcode}/", mode: 'copy'

    input:
    tuple val(barcode), path(assembly_file)
    val(db)
    val(outdir)

    output:
    tuple val(barcode), path("resfinder")

    script:
    """
    echo $barcode
    python -m resfinder -o resfinder_output -s "ecoli" -db_res $db -ifa $assembly_file -acq
    mkdir resfinder
    cp resfinder_output/pheno_table_escherichia_coli.txt resfinder/pheno_table_escherichia_coli.tsv
    cp resfinder_output/pheno_table.txt resfinder/pheno_table.tsv
    cp resfinder_output/ResFinder_results_tab.txt resfinder/resfinder_results.tsv
    cp resfinder_output/ResFinder_results.txt resfinder/resfinder_report.txt

    abricate --db resfinder $assembly_file > abricate_results.tsv
    mv abricate_results.tsv resfinder/abricate_results.tsv
    """

    stub:
    """
    mkdir resfinder
    touch resfinder/pheno_table_escherichia_coli.tsv
    touch resfinder/pheno_table.tsv
    touch resfinder/resfinder_results.tsv
    touch resfinder/resfinder_report.txt
    touch resfinder/abricate_results.tsv
    """
}
*/