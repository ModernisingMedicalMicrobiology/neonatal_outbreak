process READS_QC {
    tag {sample}

    publishDir "${params.outDir}/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy'

    input:
    tuple val(sample), path('reads.fastq.gz')

    output:
    tuple val(sample), path("${sample}.tsv")
    tuple val(sample), path("${sample}.csv")

    script:
    """
    seqkit stats -T --all reads.fastq.gz > ${sample}.tsv
    cat ${sample}.tsv | tr '\\t' ',' > ${sample}.csv

    """
    stub:
    """
    touch ${sample}.tsv
    touch ${sample}.csv
    """
}

process SUBSAMPLE {
    tag {sample}

    label 'short'

    input:
    tuple val(sample), path('reads.fastq.gz')

    output:
    tuple val(sample), path("${sample}.fastq.gz"), emit: fq

    script:
    """
    rasusa --input reads.fastq.gz --coverage 500 --genome-size 2.4m | gzip > ${sample}.fastq.gz
    """ 
    stub:
    """
    touch ${sample}.fastq.gz
    """
}

process ASSEMBLE {
    tag {sample}
    cpus 4

    label 'short'

    publishDir "${params.outDir}/assemblies/flye", mode: 'copy', pattern: 'assembly/assembly.fasta', saveAs: { filename -> "${sample}.fasta"} 
    publishDir "${params.outDir}/assemblies/flye", mode: 'copy', pattern: 'assembly/assembly_graph.gfa', saveAs: { filename -> "${sample}.gfa"}
    publishDir "${params.outDir}/assemblies/flye", mode: 'copy', pattern: 'assembly/assembly_info.txt', saveAs: { filename -> "${sample}_info.txt"}

    input:
    tuple val(sample),  file('reads.fastq.gz')

    output:
    tuple val(sample), file('assembly/assembly.fasta'), emit: fasta
    tuple val(sample), file('assembly/assembly_graph.gfa'), emit: gfa
    tuple val(sample), file('assembly/assembly_info.txt'), emit: txt

    script:
    """
    flye -o assembly \
	--plasmids \
	--meta \
	--threads ${task.cpus} \
	--nano-hq reads.fastq.gz
    """
    stub:
    """
    mkdir assembly
    touch assembly/assembly.fasta
    touch assembly/assembly.gft
    """
}

process CONTIGS_QC {
    tag {sample}

    publishDir "${params.outDir}/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy'

    input:
    tuple val(sample),  path('contigs.fasta')

    output:
    tuple val(sample),  path("${sample}.tsv")
    tuple val(sample), path("${sample}.csv")

    script:
    """
    seqkit stats -T --all contigs.fasta > ${sample}.tsv
    cat ${sample}.tsv | tr '\\t' ',' > ${sample}.csv
    """
    stub:
    """
    touch ${sample}.tsv
    touch ${sample}.csv
    """
}

process BANDAGE {
    conda "$params.bandageEnv"

    tag {sample}
    cpus 4

    label 'short'

    publishDir "${params.outDir}/assemblies/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy', saveAs: { filename -> "${sample}_image.jpg" }

    input:
    tuple val(sample), file('assembly/assembly_graph.gfa')

    output:
    tuple val(sample), file('assembly/assembly_image.jpg')

    script:
    """
    Bandage image \
    assembly/assembly_graph.gfa \
    assembly/assembly_image.jpg
    """
    stub:
    """
    touch assembly/assembly_image.jpg
    """
}

// Publish an extra symlink of the fastas so that we have unique filenames where there are duplicate barcodes in
process CIRCLATOR {
    conda "$params.circlatorEnv"

    publishDir "${params.outDir}/assemblies/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy', pattern: 'rotated_assembly.fasta', saveAs: {filename -> "${sample}.fasta"}
    publishDir "${params.outDir}/assemblies/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy', pattern: 'rotated_assembly.log', saveAs: {filename -> "${sample}.log"}

    input:
    tuple val(sample), path(txt_file), path('contigs.fasta')

    output:
    tuple val(sample), path('rotated_assembly.fasta'), emit: fasta
    tuple val(sample), path('rotated_assembly.log'), emit: log

    script:
    """
    # Create a file of non-circular contig names to ignore
    create_contig_ignore_list.py ${txt_file}

    # Only run command if ignore file was created 
    # (so text file was in the correct format to use for filtering)
    if [ -f ignore.txt ]; then
    circlator fixstart contigs.fasta rotated_assembly --ignore ignore.txt
        else
    echo "The ignore.txt file does not exist. Skipping this step"
        fi

    """
    stub:
    """
    touch ignore.txt
    touch rotated_assembly.fasta
    """
}

//process AGB {}
//Assembly Graph Browser (AGB)
// conda create -c almiheenko -c bioconda -n AGB agb
//

process POLISH {
    label 'medaka'
    tag {sample}
    cpus 2

    label 'short'

    //publishDir "${params.outDir}/assemblies/medaka", mode: 'copy', saveAs: { filename -> "${sample}.fasta"}
    publishDir "${params.outDir}/assemblies/medaka", mode: 'copy', pattern: "${sample}.fasta"
    publishDir "${params.outDir}/assemblies/medaka_renamed", mode: 'symlink', pattern: "${sample}_${batch}.fasta"

    input:
    tuple val(sample), path('contigs.fasta'), path('reads.fastq.gz')
    each path('model.tar.gz')

    output:
    //tuple val(sample), path('output/consensus.fasta'), emit: fasta
    tuple val(sample), path("${sample}.fasta"), emit: fasta
    tuple val(sample), path("${sample}_${batch}.fasta"), emit: renamed

    script:
    """
    medaka_consensus \
	-i reads.fastq.gz \
	-d contigs.fasta \
	-o output \
	-t ${task.cpus} \
	-m model.tar.gz

    mv output/consensus.fasta ${sample}.fasta

    # Create symlink with batch name included to avoid duplicate barcode confusion in merging scripts
    ln -s ${sample}.fasta ${sample}_${batch}.fasta
    """
    stub:
    """
    touch medaka/${sample}.fasta
    touch medaka_renamed/${sample}_${batch}.fasta
    """
}

process PROKKA {
    tag { sample }
    
    publishDir "${params.outDir}/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy'

    cpus 8

    input:
        tuple val(sample), path("consensus.fa")

    output:
        tuple val(sample), path("${sample}_prokka"), emit: ch_out

    script:
    """
    prokka --cpus ${task.cpus} \
        --addgenes \
        --force \
        --compliant \
        --outdir ${sample}_prokka \
        --prefix ${sample} consensus.fa
    """
    stub:
    """
    mkdir ${sample}_prokka
    touch ${sample}_prokka/${sample}.gbk 
    """
}

process BAKTA_DOWNLOAD {
    label 'online'

    output:
    path('db')

    script:
    """
    bakta_db download --output db --type light
    """
}

process BAKTA {
    tag { sample }
    errorStrategy 'ignore'
    
    publishDir "${params.outDir}/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy'

    cpus 8

    input:
        tuple val(sample), path("consensus.fa"), path('db')

    output:
        tuple val(sample), path("${sample}_bakta/${sample}.gff3"), emit: gff3
        tuple val(sample), path("${sample}_bakta"), emit: fol


    script:
    """
    bakta --db db/db-light \
        -t ${task.cpus} \
        --prefix ${sample} \
        -o ${sample}_bakta/ \
        consensus.fa
    """
    stub:
    """
    mkdir ${sample}_prokka
    touch ${sample}_prokka/${sample}.gbk 
    """
}

process ROARY {

    publishDir "${params.outDir}", mode: 'copy'

    cpus 8

    input:
    path('*')

    output:
    path("roary")

    script:
    """
    roary -p ${task.cpus} -f roary -e -n *.gff3
    """
    stub:
    """
    mkdir roary
    """
}

process ROARY_PLOTS {

    publishDir "${params.outDir}/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy'

    cpus 8

    input:
    path("roary")

    output:
    path "*"

    script:
    """
    python ${params.neoDir}/bin/roary_plots.py --labels roary/accessory_binary_genes.fa.newick  roary/gene_presence_absence.csv
    # python ${params.neoDir}/bin/roary_plots.py --labels roary/accessory_binary_genes.fa.newick  --format svg roary/gene_presence_absence.csv
    
    """
    stub:
    """
    mkdir roary_plots
    """
}

process FASTTREE {

    publishDir "${params.outDir}", mode: 'copy'

    cpus 8

    input:
    path("roary")

    output:
    file('roary/mytree.newick')

    script:
    """
    FastTree -nt -gtr roary/core_gene_alignment.aln > roary/mytree.newick
    
    """
    stub:
    """
    touch roary/mytree.newick
    """
}

process FIGTREE {
    
    publishDir "${params.outDir}", mode: 'copy'

    cpus 8

    input:
    file('roary/mytree.newick')

    output:
    file('roary/figtree_image.png')

    script:
    """
    figtree -graphic PNG roary/mytree.newick roary/figtree_image.png
    
    """
    stub:
    """
    mkdir roary/figtree
    """

}

process SPADES {
    tag {sample}
    cpus 4
    errorStrategy 'ignore'

    label 'short'

    publishDir "${params.outDir}/assemblies/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy', saveAs: { filename -> "${sample}.fasta"} 

    input:
    tuple val(sample),  file('reads1.fastq.gz'), file('reads2.fastq.gz')

    output:
    tuple val(sample), file("${sample}/contigs.fasta"), emit: fasta

    script:
    """
    spades.py -t ${task.cpus} \
            --pe1-1 reads1.fastq.gz \
            --pe1-2 reads2.fastq.gz \
            --isolate \
            -o ${sample}
    """
    stub:
    """
    mkdir ${sample}
    touch ${sample}/contigs.fasta
    """
}

process MLST {
    tag {sample}
    cpus 4

    label 'short'

    publishDir "${params.outDir}/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy'

    input:
    tuple val(sample), path("${sample}.fasta")

    output:
    tuple val(sample), path("${sample}.tsv"),  emit: 'tsv', optional: true
    tuple val(sample), path("${sample}.csv"),  emit: 'csv', optional: true

    script:
    """
    mlst ${sample}.fasta > ${sample}.tsv
    cat ${sample}.tsv | tr '\\t' ',' > ${sample}.csv
    """
    stub:
    """
    touch ${sample}.tsv
    """
}

process MASH {
    label 'online'

    publishDir "${params.outDir}/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy'

    input:
    path('*')

    output:
    path("mash_report.csv")
    path("mash_report.tsv")
    path("reference.msh")

    script:
    """
    mash sketch -p 4 *.fasta -o reference
    mash_matrix.sh reference.msh
    cat mash_report.tsv | tr '\\t' ',' > mash_report.csv
    """
    stub:
    """
    mkdir mash
    touch mash/reference.msh
    touch mash/mash_report.tsv
    """
}


// don't include this process for now
process AMRFINDER_DOWNLOAD {
    label 'online'

    publishDir "${params.outDir}/${task.process.replaceAll(":","_").toLowerCase()}", mode: 'copy'

    output:
    path("${params.amrfinder_db_version}"), optional: true, emit: db

    script:
    """
    # Only download database if it does not exist already
    if [ -d "${params.amrfinder_db}" ]; then
        echo "The amrfinder database version ${params.amrfinder_db_version} already exists."
    else
        echo "Downloading amrfinder database version ${params.amrfinder_db_version} ..."
        amrfinder_update -d "${params.amrfinder_dbDir}" --version ${params.amrfinder_db_version}
    fi
    """

    stub:
    """
    if [ ! -d "${params.amrfinder_db}" ]; then
        mkdir -p ${params.amrfinder_db}
        ln -s ${params.amrfinder_db} ${params.amrfinder_dbDir}/latest
    fi
    """
}






