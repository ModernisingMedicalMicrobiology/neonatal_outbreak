#!/usr/bin/awk -f

# Check if a filename is provided as a command-line argument
BEGIN {
    if (ARGC < 2) {
        print "Usage: " ARGV[0] " <filename>"
        exit 1
    }
}

# Process each row
{
    # Check if it's the header row
    if (NR == 1) {
        # Check if the 4th column header is "circ."
        if ($4 != "circ.") {
            print "Error: Info file is not in the required format."
            exit 1;  # Exit with an error code
        }
    } else {
        # Check if the 4th column is "circ."
        if ($4 == "N") {
            # Remove underscores from $1 and append to the ignore.txt file
            print $1 >> "ignore.txt";
        }
    }
}
