#!/usr/bin/env python3

import sys
import os

# Check if a filename is provided as a command-line argument
if len(sys.argv) < 2:
    print("Usage: {} <filename>".format(sys.argv[0]))
    sys.exit(1)

filename = sys.argv[1]

# Process each row
with open(filename, 'r') as file:
    for line_num, line in enumerate(file, start=1):
        row = line.strip().split()

        # Check if it's the header row
        if line_num == 1:
            # Check if the 4th column header is "circ."
            if row[3] != "circ.":
                print("Error: Info file is not in the required format.")
                sys.exit(1)
        else:
            # Create an empty ignore.txt file if it doesn't exist
            if not os.path.exists("ignore.txt"):
                open("ignore.txt", 'a').close()
            # Check if the 4th column is N
            if row[3] == "N":
                # Remove underscores from the first column and append to the ignore.txt file
                with open("ignore.txt", 'a') as ignore_file:
                    ignore_file.write(row[0] + '\n')
    