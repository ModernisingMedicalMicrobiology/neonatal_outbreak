# Taken from Matthew's serotyping_ecoli pipeline

import argparse
from pathlib import Path
import pandas as pd
import ast
import os

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', "--amr_reports", help="folder containing combined csvs")
    parser.add_argument('-o', "--outfile", help="csv file summarising all samples")
    parser.add_argument('-d', "--data_group", help="name of data group")
    args = parser.parse_args()

    dfs = []
    for file_name in ast.literal_eval(args.amr_reports):
        print(file_name)
        df = pd.read_csv(file_name, sep='\t')

        df['ID'] = os.path.basename(file_name).replace('_amr.tsv', '')
        df['data_group'] = args.data_group
        dfs.append(df)

    df = pd.concat(dfs)
    
    df = df[['data_group', 'ID', 'Gene symbol', 'Sequence name', 'Class', 'Subclass']]
    df = df.sort_values(['data_group', 'ID', 'Gene symbol'])
    df.to_csv(args.outfile, index=False)