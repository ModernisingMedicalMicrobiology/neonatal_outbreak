#!/bin/bash

# Taken from https://github.com/marbl/Mash/issues/180
ref=$1

echo $ref

#mash sketch -p 4 $fasta_dir/*.fasta -o $reference_dir
mash triangle -p 4 $ref | awk 'NR == 1 {n=$1}
                function basename(file, a, n) {
                  n = split(file, a, "/")
                  return a[n]
                }
                NR > 1 {i=NR-1; names[i] = basename($1);
                  for (j=2; j <= NF; j++){
                    mat[i,j-1] = mat[j-1,i] = $j;
                  }
                  mat[i,i]=0.0;
                }
                END{i=1;
                  for (a=1; a<=length(names); a++){printf "\t"names[a]}; printf "\n";
                  for (a=1; a<=length(names); a++){
                    printf names[a];
                    for(j=1; j<=n; j++)
                      printf "\t%f", mat[i,j];
                    printf "\n";
                    i++
                  }
                }' > mash_report.tsv