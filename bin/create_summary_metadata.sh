#!/usr/bin/env Rscript

# This script takes the pipeline results and combines them in one large metadata table
# As we are using nextflow, we will make a row for each sample and combine these rows later
# Actually we cant do this because we need all the plasmid and gene data to make this work

# Results we want:
# Barcode
# Batch
# MLST 7 columns
# Plasmid columns with 1s and 0s
# Gene list


# script.R

# Get command line arguments
args <- commandArgs(trailingOnly = TRUE)

# Check if there are at least two arguments (script.R and argument1)
if (length(args) < 2) {
  stop("Usage: script.R argument1")
}

# Extract the paths from the arguments
paths <- tail(args, -1)


# Open a text file for writing
output_file <- "output_paths.txt"
cat("Paths:\n", file = output_file)

# Print and write each path to the text file
for (i in seq_along(paths)) {
  path <- paths[i]
  
  # Print the path to console
  cat("Path ", i, ": ", path, "\n")
  
  # Write the path to the text file
  cat("Path ", i, ": ", path, "\n", file = output_file, append = TRUE)
}

# Close the text file
close(output_file)



# Load each path into a separate data frame or data table
# for (i in seq_along(paths)) {
#   path <- paths[i]
  
#   df <- read.csv(path)

# }
